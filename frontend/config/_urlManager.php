<?php
use \yii\web\Request;
$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());

return [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'baseUrl' => $baseUrl,
    'rules' => [
        ['pattern' => 'sitemap.xml', 'route' => 'site/sitemap'],
        ['pattern' => 'page/<alias>', 'route' => 'page/view'],
        ['pattern' => '<alias>', 'route' => 'site/<alias>'],
        ['pattern' => 'projects/<alias>', 'route' => 'project/<alias>'],
    ]
];
