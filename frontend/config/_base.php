<?php
use \yii\web\Request;
$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());

return [
    'id' => 'frontend',
    'basePath'=>dirname(__DIR__),
    'components' => [
        'request' => ['baseUrl' => $baseUrl,],
        'urlManager'=>require(__DIR__.'/_urlManager.php'),
    ],
];