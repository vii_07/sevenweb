<?php
/* @var $this yii\web\View */
?>
<?php
$this->title = Yii::$app->name . ' - ' . Yii::t('frontend', 'Projects');
$this->registerCssFile('/css/project.css');
?>
<div class="alert alert-info">
    <h1 class="title-center title-upper-case"><?php echo Yii::t('_project', 'My projects'); ?></h1>
    <div class="row">
        <div class="col-lg-3 centertext">
            <a href="http://vii07.ho.ua/scroll/" target="_blank">
                <img class="item-image" src="/img/projects/InternetShop.png" alt="Internet Shop">
                <?php echo Yii::t('_project', 'Internet Shop'); ?>
            </a>
        </div>
        <div class="col-lg-3 centertext">
            <a href="http://vii07.ho.ua/formaphp/" target="_blank">
                <img class="item-image" src="/img/projects/ValidateForm.png" alt="Validate Form">
                <?php echo Yii::t('_project', 'Validate Form'); ?>
            </a>
        </div>
        <div class="col-lg-3 centertext">
            <a href="javascript:void(0);" target="_blank" title="<?php echo Yii::t('_project', 'Temporarily not working'); ?>">
                <img class="item-image" src="/img/projects/MOBD.png" alt="Seven Web">
                <?php echo Yii::t('_project', 'Invite to Event'); ?></a>
        </div>
        <div class="col-lg-3 centertext">
            <a href="javascript:void(0);" target="_blank" title="<?php echo Yii::t('_project', 'Temporarily not working'); ?>">
                <img class="item-image" src="/img/projects/Toronto.png" alt="Seven Web">
                <?php echo Yii::t('_project', 'Parse event'); ?>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 centertext">
            <a href="http://vii07.ho.ua/about.php" target="_blank" title="<?php echo Yii::t('_project', 'Temporarily not updating'); ?>">
                <img class="item-image" src="/img/projects/PrevPortfolio.png" alt="Previous Site-Portfolio">
                <?php echo Yii::t('_project', 'Previous Site-Portfolio'); ?>
            </a>
        </div>
        <div class="col-lg-3 centertext">
            <a href="http://www.atlaslifttech.com/" target="_blank">
                <img class="item-image" src="/img/projects/Atlas.png" alt="Hospital's personnel management">
                <?php echo Yii::t('_project', 'Hospital\'s personnel management'); ?>
            </a>
        </div>
        <div class="col-lg-3 centertext">
            <a href="http://dolocal.org/" target="_blank">
                <img class="item-image" src="/img/projects/Dolocal.png" alt="Request Service Dolocal">
                <?php echo Yii::t('_project', 'Request Service Dolocal'); ?>
            </a>
        </div>
        <div class="col-lg-3 centertext">
            <a href="http://sonic-solutions.dev.lezgro.com/" target="_blank">
                <img class="item-image" src="/img/projects/Sonic.png" alt="Sonic Solutions">
                <?php echo Yii::t('_project', 'Sonic Solutions'); ?>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 centertext">
            <a href="http://idees.ru/" target="_blank">
                <img class="item-image" src="/img/projects/Idees.png" alt="IDEES.RU">
                <?php echo Yii::t('_project', 'IDEES.RU'); ?>
            </a>
        </div>
        <div class="col-lg-3 centertext">
            <a href="http://app.jellable.com/public-test-intro" target="_blank">
                <img class="item-image" src="/img/projects/JELL.png" alt="JELLABLE">
                <?php echo Yii::t('_project', 'Discover your sociotype'); ?>
            </a>
        </div>
        <div class="col-lg-3 centertext">
            <a href="http://classicogaming.tk" target="_blank">
                <img class="item-image" src="/img/projects/ClassicoGaming.png" alt="Classico Gaming">
                <?php echo Yii::t('_project', 'Classico Gaming'); ?>
            </a>
        </div>
        <div class="col-lg-3 centertext">
            <a href="javascript:void(0);">
                <img class="item-image" src="/img/projects/Advertising.jpg" alt="It could be your site">
                <?php echo Yii::t('_project', 'It could be your site'); ?>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="agreement">
            <?php //echo Yii::t('_project', 'And many other corporative projects which are blocked by non-disclosure agreement...'); ?>
        </div>
    </div>
</div>
