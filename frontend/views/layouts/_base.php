<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

/* @var $this \yii\web\View */
/* @var $content string */

\frontend\assets\FrontendAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <meta name="keywords" content="Seven, Web, Seven Web, Development, Розробка сайтів, PHP, Yii, HTML, HTML5, CSS, CSS3, Bootstrap, JavaScript, Jquery, Ajax, XML/JSON">
        <meta name="description" content="My name is Ivan Vovchak. I am backend web developer. But I already have skills for frontend development too.">
        <meta name="author" content="Ivan Vovchak">
        <meta name="copyright" content="Ivan Vovchak">
        <meta name="robots" content="index, follow">
        <meta http-equiv="content-type" content="text/html;UTF-8">
        <meta http-equiv="cache-control" content="no-cache">
        <meta http-equiv="content-language" content="en">
    </head>
    <body>

        <?php $this->beginBody() ?>
        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => '<img height="25" width="auto" src="/img/company/logotip_white.png" alt="Seven Web">', //Yii::t('frontend', 'Portfolio'),
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    ['label' => Yii::t('frontend', 'Home'), 'url' => ['/site/index']],
                    ['label' => Yii::t('frontend', 'About me'), 'url' => ['/site/about']],
                    ['label' => Yii::t('frontend', 'Projects'), 'url' => ['/project/index']],
                    ['label' => Yii::t('frontend', 'Contact'), 'url' => ['/site/contact']],
                    /*
                     * need to work with this items
                      ['label' => Yii::t('frontend', 'About'), 'url' => ['/page/view', 'alias' => 'about']],
                      ['label' => Yii::t('frontend', 'Articles'), 'url' => ['/article/index']],
                      ['label' => Yii::t('frontend', 'Contact'), 'url' => ['/site/contact']],
                      ['label' => Yii::t('frontend', 'Signup'), 'url' => ['/user/sign-in/signup'], 'visible' => Yii::$app->user->isGuest],
                      ['label' => Yii::t('frontend', 'Login'), 'url' => ['/user/sign-in/login'], 'visible' => Yii::$app->user->isGuest],
                      [
                      'label' => Yii::$app->user->isGuest ? '' : Yii::$app->user->identity->getPublicIdentity(),
                      'visible' => !Yii::$app->user->isGuest,
                      'items' => [
                      [
                      'label' => Yii::t('frontend', 'Account'),
                      'url' => ['/user/default/index']
                      ],
                      [
                      'label' => Yii::t('frontend', 'Profile'),
                      'url' => ['/user/default/profile']
                      ],
                      [
                      'label' => Yii::t('frontend', 'Backend'),
                      'url' => Yii::getAlias('@backendUrl'),
                      'visible' => Yii::$app->user->can('manager')
                      ],
                      [
                      'label' => Yii::t('frontend', 'Logout'),
                      'url' => ['/user/sign-in/logout'],
                      'linkOptions' => ['data-method' => 'post']
                      ]
                      ]
                      ],
                     */
                    [
                        'label' => Yii::t('frontend', 'Language'),
                        'items' => array_map(function($code) {
                                    return [
                                        'label' => Yii::$app->params['availableLocales'][$code],
                                        'url' => ['/site/set-locale', 'locale' => $code],
                                        'active' => Yii::$app->language == $code
                                    ];
                                }, array_keys(Yii::$app->params['availableLocales']))
                            ]
                        ],
                    ]);
                    NavBar::end();
                    ?>

                    <?= $content ?>

                </div>

                <div id="footer">
                    <div id="copy">
                        <div id="technologies">
                            <ul class="technologies">
                                <li>
                                    <a target="_blank" href="https://wordpress.org/">
                                        <img height="75" width="auto" src="/img/technologies/wp.jpg" alt="https://wordpress.org/">
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="http://joomla.org/">
                                        <img height="75" width="auto" src="/img/technologies/technology1.png" alt="http://joomla.org/">
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="http://git-scm.com/">
                                        <img height="75" width="auto" src="/img/technologies/git.png" alt="http://git-scm.com/">
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="http://jquery.com/">
                                        <img height="75" width="auto" src="/img/technologies/technology3.png" alt="http://jquery.com/">
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="http://javascript.ru/">
                                        <img height="75" width="auto" src="/img/technologies/js.png" alt="http://javascript.ru/">
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="http://htmlbook.ru/html">
                                        <img height="75" width="auto" src="/img/technologies/technology13.png" alt="http://htmlbook.ru/html">
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="http://htmlbook.ru/css">
                                        <img height="75" width="auto" src="/img/technologies/technology14.png" alt="http://htmlbook.ru/css">
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="http://www.postgresql.org/">
                                        <img height="75" width="auto" src="/img/technologies/pg.png" alt="http://www.postgresql.org/">
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="http://mysql.com/">
                                        <img height="75" width="auto" src="/img/technologies/technology9.png" alt="http://mysql.com/">
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="http://php.net/">
                                        <img height="75" width="auto" src="/img/technologies/technology10.png" alt="http://php.net/">
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="http://yiiframework.com/">
                                        <img height="75" width="auto" src="/img/technologies/technology7.png" alt="http://yiiframework.com/">
                                    </a>
                                </li>
                            </ul>                        
                        </div>
                    </div>
                </div>
                <footer class="footer">
                    <div>
                        <div class="container">
                            <p class="pull-left">&copy; VII <?= date('Y') ?></p>
                            <p class="pull-right"><?= Yii::powered() ?></p>
                        </div>
                    </div>    
                </footer>

                <?php $this->endBody() ?>
            </body>
        </html>
        <?php $this->endPage() ?>
