<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = Yii::$app->name . ' - ' . Yii::t('_contact', 'Contacts');
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact alert alert-info">
    <h1 class="title-center title-upper-case"><?= Html::encode(Yii::t('_contact', 'Contacts')) ?></h1>

    <div class="row">
        <dl class="cont">
            <dt><img src="/img/tel.png" width="69" height="62"></dt>
            <dd><?php echo Yii::$app->params['adminPhone'] ?></dd>
            <dt>
            <a href="mailto:<?php echo Yii::$app->params['adminEmail'] ?>">
                <img src="/img/email.png" alt="email" width="69" height="62">
            </a>
            </dt>
            <dd>
                <?php echo Yii::$app->params['adminEmail'] ?><br>
                <?php echo Yii::$app->params['adminEmailSecond'] ?>
            </dd>
            <dt>
            <a href="skype:<?php echo Yii::$app->params['adminSkypeLogin'] ?>?call">
                <img src="/img/skype_big.png" alt="skype" width="62" height="62" style="margin-left: 4px;">
            </a>
            </dt>
            <dd><?php echo Yii::$app->params['adminSkypeLogin'] ?></dd>
            <dt></dt>
            <dd>
                <div class="contacts p-30 social-services" style="margin-bottom:25px;">
                    <script type="text/javascript" src="http://download.skype.com/share/skypebuttons/js/skypeCheck.js"></script>
                    <a target="_blank" href="<?php echo Yii::$app->params['adminFacebookUrl'] ?>"><img src="/img/f.png" width="20" height="19"></a>
                    <a target="_blank" href="<?php echo Yii::$app->params['adminVkUrl'] ?>"><img src="/img/b.png" width="20" height="19"></a>
                    <a href="skype:<?php echo Yii::$app->params['adminSkypeLogin'] ?>?call"><img src="/img/s.png" width="20" height="19" alt="Skype Me™!"></a>
                </div>
            </dd>
        </dl>
    </div>

    <p class="about-row send"><?php echo Yii::t('_contact', 'If you have business inquiries or other questions, please fill out the following form to contact us.') ?></p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'subject') ?>
            <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>
            <?=
            $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-4" style="margin-left: 2%;">{input}</div></div>',
            ])
            ?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('_contact', 'Send'), ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-lg-2"></div>
        <div class="col-lg-3">
            <div class="block bg title-center pad">
                <span style="font-size:18px;font-weight:bold;margin-bottom:25px;display:block;"><?php echo Yii::t('_contact', 'Call us') ?>:</span>
                <span style="font-size:14px;margin-bottom:25px;display:block;"><?php echo Yii::$app->params['adminPhone'] ?></span>
                <div class="contacts" style="margin-bottom:25px;">
                    <a href="skype:<?php echo Yii::$app->params['adminSkypeLogin'] ?>?call"><img src="/img/s.png" width="20" height="19" alt="Skype Me™!"></a>
                    <a href="mailto:<?php echo Yii::$app->params['adminEmail'] ?>"><img src="/img/mail.png" alt="mail" width="17" height="16"></a>
                </div>
            </div>
        </div>
    </div>

</div>
