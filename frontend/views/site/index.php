<?php
/* @var $this yii\web\View */
$this->title = Yii::$app->name;
?>
<div class="site-index">

    <div class="jumbo alert alert-info">
        <h1><?php echo Yii::t('frontend', 'Hello!'); ?></h1>
        <p class="lead">
            <?php echo Yii::t('frontend', 'My name is Ivan Vovchak.'); ?>
            <?php echo Yii::t('frontend', 'I am backend web developer.'); ?>
        </p>
        <p class="lead"><?php echo Yii::t('frontend', 'But I already have skills for frontend development too.'); ?></p>
        <p class="lead"><?php echo Yii::t('frontend', 'You can view more of my skills at slider below.'); ?></p>

    </div>

    <div class="alert alert-info">
        <?=
        \common\components\widgets\carousel\CarouselWidget::widget([
            'alias' => 'index'
        ])
        ?>
    </div>

    <!--    <div class="body-content">
    
            <div class="row">
                <div class="col-lg-4">
                    <h2>Heading</h2>
    
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur.</p>
    
                    <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
                </div>
                <div class="col-lg-4">
                    <h2>Heading</h2>
    
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur.</p>
    
                    <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
                </div>
                <div class="col-lg-4">
                    <h2>Heading</h2>
    
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur.</p>
    
                    <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
                </div>
            </div>
    
        </div>-->
</div>
