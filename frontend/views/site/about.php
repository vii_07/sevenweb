<?php $this->title = Yii::$app->name . ' - ' . Yii::t('frontend', 'About me'); ?>
<div class="alert alert-info">
    <h1 class="title-center title-upper-case"><?php echo Yii::t('frontend', 'About me'); ?></h1>
    <div class="row">
        <div class="col-lg-4">
            <h2><?php echo Yii::t('_about', 'Personal data'); ?></h2>
            <blockquote class="about-row">
                <p class="about-row-bold"><?php echo Yii::t('_about', 'Name: Ivan Vovchak'); ?></p>
                <p><?php echo Yii::t('_about', 'Ivano-Frankivsk, Ukraine'); ?></p>
                <p><?php echo Yii::t('_about', 'Phone'); ?>: +38 (098) 705-00-91</p>
                <p>Email: vii_07@ukr.net</p>
                <p>Skype: vii_07</p>
            </blockquote>
        </div>
        <div class="col-lg-4">
            <h2><?php echo Yii::t('_about', 'Education'); ?></h2>
            <blockquote class="about-row">
                <p><?php echo Yii::t('_about', 'Carpathian National University named after Vasyl Stefanyk.'); ?></p>
                <p><?php echo Yii::t('_about', 'Faculty of Mathematics and Computer Science.'); ?></p>
            </blockquote>
        </div>
        <div class="col-lg-4">
            <h2><?php echo Yii::t('_about', 'Knowledge'); ?></h2>
            <blockquote class="about-row">
                <p>HTML, HTML5, CSS, CSS3, Bootstrap</p>
                <p>JavaScript, Jquery, Ajax, XML/JSON</p>
                <p>PHP, Coding Standart, OOP, Patterns</p>
                <p>Yii and Yii2 Frameworks, REST API</p>
                <p>MySQL, PostgreSQL</p>
                <p>GIT, Apache, Nginx</p>
                <p>Wordpress, Joomla</p>
            </blockquote>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <h2><?php echo Yii::t('_about', 'Languages'); ?></h2>
            <blockquote class="about-row">
                <p><?php echo Yii::t('_about', 'Ukrainian (native)'); ?></P>
                <p><?php echo Yii::t('_about', 'English (intermediate)'); ?></p>
                <p><?php echo Yii::t('_about', 'Polish'); ?></p>
                <p><?php echo Yii::t('_about', 'Russian'); ?></p>
            </blockquote>
        </div>
        <div class="col-lg-4">
            <h2><?php echo Yii::t('_about', 'Work experience'); ?></h2>
            <blockquote class="about-row">
                <p>
                    <?php echo Yii::t('_about', 'Freelancer'); ?><br />
                    <?php echo Yii::t('_about', 'Role: '); ?><?php echo Yii::t('_about', 'Web Developer'); ?><br />
                    <?php echo Yii::t('_about', 'Duration: '); ?><?php echo Yii::t('_about', '1 year 5 months'); ?><br />
                </p>
                <p>
                    <?php echo Yii::t('_about', 'Company name: '); ?>"Lezgro"<br />
                    <?php echo Yii::t('_about', 'Role: '); ?><?php echo Yii::t('_about', 'Web Developer'); ?><br />
                    <?php echo Yii::t('_about', 'Duration: ') . $duratiomLezgro; ?><br />
                </p>
            </blockquote>
        </div>
        <div class="col-lg-4">
            <h2><?php echo Yii::t('_about', 'Personal qualities'); ?></h2>
            <blockquote class="about-row">
                <p><?php echo Yii::t('_about', 'Ability to work well and quickly.'); ?></p>
                <p><?php echo Yii::t('_about', 'Integrity at work.'); ?></p>
                <p><?php echo Yii::t('_about', 'The ability to quickly acquire new knowledge.'); ?></p>
                <p><?php echo Yii::t('_about', 'A willingness to work and earn.'); ?></p>
            </blockquote>
        </div>
    </div>
</div>

