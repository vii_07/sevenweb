<?php
header('Content-type: application/xml; charset="utf-8"',true);
$xml = '<?xml version="1.0" encoding="utf-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<url>
	<loc>http://sevenweb.zz.mu/</loc>
	<lastmod>' . date("Y-m-d") . '</lastmod>
	<changefreq>always</changefreq>
	<priority>0.5</priority>
</url>
<url>
	<loc>http://sevenweb.zz.mu/about</loc>
	<lastmod>' . date("Y-m-d") . '</lastmod>
	<changefreq>always</changefreq>
	<priority>0.5</priority>
</url>
<url>
	<loc>http://sevenweb.zz.mu/projects/index</loc>
	<lastmod>' . date("Y-m-d") . '</lastmod>
	<changefreq>always</changefreq>
	<priority>0.5</priority>
</url>
<url>
	<loc>http://sevenweb.zz.mu/contact</loc>
	<lastmod>' . date("Y-m-d") . '</lastmod>
	<changefreq>always</changefreq>
	<priority>0.5</priority>
</url>
</urlset>';

echo $xml;
Yii::$app->end();
?>