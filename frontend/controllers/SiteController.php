<?php

namespace frontend\controllers;

use Yii;
use frontend\models\ContactForm;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'set-locale' => [
                'class' => 'common\components\action\SetLocale',
                'locales' => array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->contact(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                        'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        $duratiomLezgro = $this->getTimeDuration('2013-07-22');
        return $this->render('about', ['duratiomLezgro' => $duratiomLezgro]);
    }
    
    public function actionSitemap()
    {
        return $this->render('sitemap');
    }

    private function getTimeDuration($date1, $date2 = null)
    {
        $datetime1 = date_create($date1);
        $datetime2 = $date2 = null ? date_create(date()) : date_create($date2);
        $interval = date_diff($datetime1, $datetime2);
        $strInterval = '';
        switch (true) {
            case $interval->y == 0:
                break;
            case $interval->y == 1:
                if (Yii::$app->language == 'uk-UA') {
                    $strInterval .= $interval->y . ' рік ';
                }
                if (Yii::$app->language == 'en-US') {
                    $strInterval .= $interval->y . ' year ';
                }
                if (Yii::$app->language == 'ru-RU') {
                    $strInterval .= $interval->y . ' год ';
                }
                break;
            case $interval->y < 5:
                if (Yii::$app->language == 'uk-UA') {
                    $strInterval .= $interval->y . ' роки ';
                }
                if (Yii::$app->language == 'en-US') {
                    $strInterval .= $interval->y . ' years ';
                }
                if (Yii::$app->language == 'ru-RU') {
                    $strInterval .= $interval->y . ' года ';
                }
                break;
            default:
                if (Yii::$app->language == 'uk-UA') {
                    $strInterval .= $interval->y . ' років ';
                }
                if (Yii::$app->language == 'en-US') {
                    $strInterval .= $interval->y . ' years ';
                }
                if (Yii::$app->language == 'ru-RU') {
                    $strInterval .= $interval->y . ' годов ';
                }
                break;
        }
        switch (true) {
            case $interval->m == 0:
                break;
            case $interval->m == 1:
                if (Yii::$app->language == 'uk-UA') {
                    $strInterval .= $interval->m . ' місяць';
                }
                if (Yii::$app->language == 'en-US') {
                    $strInterval .= $interval->m . ' month';
                }
                if (Yii::$app->language == 'ru-RU') {
                    $strInterval .= $interval->m . ' месяц';
                }
                break;
            case $interval->m < 5:
                if (Yii::$app->language == 'uk-UA') {
                    $strInterval .= $interval->m . ' місяці';
                }
                if (Yii::$app->language == 'en-US') {
                    $strInterval .= $interval->m . ' months';
                }
                if (Yii::$app->language == 'ru-RU') {
                    $strInterval .= $interval->m . ' месяца';
                }
                break;
            default:
                if (Yii::$app->language == 'uk-UA') {
                    $strInterval .= $interval->m . ' місяців';
                }
                if (Yii::$app->language == 'en-US') {
                    $strInterval .= $interval->m . ' months';
                }
                if (Yii::$app->language == 'ru-RU') {
                    $strInterval .= $interval->m . ' месяцов';
                }
                break;
        }
        return $strInterval;
    }

}
