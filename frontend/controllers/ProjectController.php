<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;

class ProjectController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

}
