<?php
return [
    'components' => [
        'cache' => [
            'class' => 'yii\caching\DummyCache',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=127.0.0.1;dbname=u600322495_web7', // localhost is much slower than 127.0.0.1
            'username' => 'u600322495_root',
            'password' => '0707ivan',
            'charset' => 'utf8'
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
            'messageConfig' => [
                'charset' => 'UTF-8',
                'from' => 'support@sevenweb.zz.mu',
            ]
        ],
    /* 'log'=>[
            'targets'=>[
                'email' => [
                'class' => 'yii\log\EmailTarget',
                'except' => ['yii\web\HttpException:404'],
                'levels' => ['error', 'warning'],
                'message' => ['from' => 'robot@example.com', 'to' => 'admin@example.com'],
                ]
            ]
      ] */
    ]
];
