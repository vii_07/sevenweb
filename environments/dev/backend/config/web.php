<?php
return \yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/_base.php'),
    [
        'components' => [
            'request' => [
                // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
                'cookieValidationKey' => 'kXLHh-FcSoD0qTfHFTzdgPbZ1KnNwuCP',
            ],
        ],
    ]
);