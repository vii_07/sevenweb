<?php

//For frontend view folder "project"
return [
    'My projects' => 'Мої проекти',
    'Internet Shop' => 'Інтернет-магазин',
    'Validate Form' => 'Перевірити Форму',
    'Invite to Event' => 'Запрошення на подію',
    'Parse event' => 'Парсинг події',
    'And many other corporative projects which are blocked by non-disclosure agreement...' => 'І багато інших корпоративних проектів, які блокуються за угодою про нерозголошення...',
    'Temporarily not working' => 'Тимчасово не працює',
    'Temporarily not updating' => 'Тимчасово не оновлюється',
    'Previous Site-Portfolio' => 'Моє попереднє портфоліо',
    'Hospital\'s personnel management' => 'Управління персоналом лікарень',
    'Request Service Dolocal' => 'Служба заявок Dolocal',
    'Discover your sociotype' => 'Визначити свій соціотип',
    'It could be your site' => 'Тут може бути ваш сайт',
];