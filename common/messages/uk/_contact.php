<?php

//For frontend page "Contacts"
return [
    'Contacts' => 'Контакти',
    'If you have business inquiries or other questions, please fill out the following form to contact us.' => 'Якщо у вас є ділова пропозиція або інші питання, будь ласка, заповніть наступну форму, щоб зв\'язатися з нами.',
    'Send' => 'Відправити',
    'Call us' => 'Зателефонуйте нам',
];
