<?php

//For frontend page "Contacts"
return [
    'Contacts' => 'Контакты',
    'If you have business inquiries or other questions, please fill out the following form to contact us.' => 'Если у вас есть деловое предложение или другие вопросы, пожалуйста, заполните следующую форму, чтобы связаться с нами.',
    'Send' => 'Отправить',
    'Call us' => 'Позвоните нам',
];
