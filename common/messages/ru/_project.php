<?php

//For frontend view folder "project"
return [
    'My projects' => 'Мои проекты',
    'Internet Shop' => 'Интернет-магазин',
    'Validate Form' => 'Проверить Форму',
    'Invite to Event' => 'Приглашение на мероприятие',
    'Parse event' => 'Парсинг мероприятия',
    'And many other corporative projects which are blocked by non-disclosure agreement...' => 'И много других корпоративных проектов, которые блокируются по соглашению о неразглашении...',
    'Temporarily not working' => 'Временно не работает',
    'Temporarily not updating' => 'Временно не обновляется',
    'Previous Site-Portfolio' => 'Мое предыдущее портфолио',
    'Hospital\'s personnel management' => 'Управление персоналом больниц',
    'Request Service Dolocal' => 'Служба заявок Dolocal',
    'Discover your sociotype' => 'Определить свой социотип',
    'It could be your site' => 'Здесь может быть ваш сайт',
];