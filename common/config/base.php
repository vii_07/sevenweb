<?php
return [
    'name'=>'Seven Web',
    'vendorPath'=>dirname(dirname(__DIR__)).'/vendor',
    'extensions' => require(__DIR__ . '/../../vendor/yiisoft/extensions.php'),
    'sourceLanguage'=>'en-US',
    'language'=>'en-US',
    'bootstrap' => ['log'],
    'components' => [

        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'itemTable' => 'rbac_auth_item',
            'itemChildTable' => 'rbac_auth_item_child',
            'assignmentTable' => 'rbac_auth_assignment',
            'ruleTable' => 'rbac_auth_rule',
            'defaultRoles' => ['administrator', 'manager', 'user'],
        ],

        'cache' => [
            'class' => 'yii\caching\FileCache',
            //'class' => 'yii\caching\DummyCache',
            //'keyPrefix'=>'seven-web'
        ],

        'formatter'=>[
            'class'=>'yii\i18n\Formatter'
        ],

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'messageConfig' => [
                'charset' => 'UTF-8'
            ]
        ],

        'db'=>[
            'class'=>'yii\db\Connection'
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                'db'=>[
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['error', 'warning'],
                    'except'=>['yii\web\HttpException:*', 'yii\i18n\I18N\*'],
                    'prefix'=>function(){
                        $url = !Yii::$app->request->isConsoleRequest ? Yii::$app->request->getUrl() : null;
                        return sprintf('[%s][%s]', Yii::$app->id, $url);
                    },
                    'logVars'=>[],
                    'logTable'=>'{{%system_log}}'
                ]
            ],
        ],

        'i18n' => [
            'translations' => [
                'app'=>[
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath'=>'@common/messages',
                ],
                '*'=> [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath'=>'@common/messages',
                    'fileMap'=>[
                        'common'=>'common.php',
                        'backend'=>'backend.php',
                        'frontend'=>'frontend.php',
                    ]
                ],
                /*
                 '*'=> [
                    'class' => 'yii\i18n\DbMessageSource',
                    'sourceMessageTable'=>'{{%i18n_source_message}}',
                    'messageTable'=>'{{%i18n_message}}',
                    'enableCaching' => true,
                    'cachingDuration' => 3600
                ],
                */

            ],
        ],

        'fileStorage'=>[
            'class'=>'trntv\filekit\storage\FileStorage',
            'repositories'=>[
                'uploads'=>[
                    'class'=>'trntv\filekit\storage\repository\FilesystemRepository',
                    'basePath'=>'@storage',
                    'baseUrl'=>'@storageUrl',
                ],
                'tmp'=>[
                    'class'=>'trntv\filekit\storage\repository\FilesystemRepository',
                    'basePath'=>'@storage',
                    'baseUrl'=>'@storageUrl',
                ]
            ],

        ],

        'keyStorage'=>[
            'class'=>'common\components\keyStorage\KeyStorage'
        ],

        'urlManagerBackend'=>\yii\helpers\ArrayHelper::merge(
            [
                'hostInfo'=>Yii::getAlias('@backendUrl')
            ],
            require(Yii::getAlias('@backend/config/_urlManager.php'))
        ),
        'urlManagerFrontend'=>\yii\helpers\ArrayHelper::merge(
            [
                'hostInfo'=>Yii::getAlias('@frontendUrl')
            ],
            require(Yii::getAlias('@frontend/config/_urlManager.php'))
        ),
    ],
    'params' => [
        'adminEmail' => 'vii_07@ukr.net',
        'adminEmailSecond' => 'vii_07@i.ua',
        'adminSkypeLogin' => 'vii_07',
        'adminPhone' => '+38 (098) 705 00 91',
        'adminFacebookUrl' => 'https://www.facebook.com/ivan.vovchak',
        'adminVkUrl' => 'https://vk.com/vii_07',
        'availableLocales'=>[
            'en-US'=>'English (US)',
            'ru-RU'=>'Русский (РФ)',
            'uk-UA'=>'Українська (Україна)'
        ],
    ],
];
